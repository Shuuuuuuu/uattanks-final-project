﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    //All Variables
    public int players;
    public Rigidbody shell;
    public Transform fireStart;
    public Slider aimSlider;
    public AudioSource shootingAudio;
    public AudioClip ChargingClip;
    public AudioClip FireClip;
    public float minLaunchForce = 15f;
    public float maxLaunchForce = 30f;
    public float maxChargeTime = 0.75f;
    public float nextFire;
    public float fireRate;
    private string fireButton;
    private float currentLaunchForce;
    private float chargeSpeed;
    private bool fired;

    //Find our fire from our input and our charge speed whel firing
   private void Start()
   {
        fireButton = "Fire" + players;
        chargeSpeed = (maxLaunchForce - minLaunchForce) / maxChargeTime;
   }

    // Update is called once per frame
    void Update()
    {
        //Gets our aimSlider and tells us in various if statements how our arrow will pop up and charge our shots
        aimSlider.value = minLaunchForce;
        if (currentLaunchForce >= maxLaunchForce && !fired)
        {
            currentLaunchForce = maxLaunchForce;
            Fire(currentLaunchForce, 1);
        }

        else if (Input.GetButtonDown(fireButton) && Time.time > nextFire)
        {
                nextFire = Time.time + fireRate;
                fired = false;
                currentLaunchForce = minLaunchForce;
                shootingAudio.clip = ChargingClip;
                shootingAudio.Play();
            
        }

        else if (Input.GetButton(fireButton) && !fired)
        {
            currentLaunchForce += chargeSpeed * Time.deltaTime;
            aimSlider.value = currentLaunchForce;
        }

        else if (Input.GetButtonUp(fireButton) && !fired)
        {
            Fire(currentLaunchForce, 1);
        }
    }

    private void OnEnable()
    {
        currentLaunchForce = minLaunchForce;
        aimSlider.value = minLaunchForce;
    }

    //When we Fire our shell will have a clone and play its audio
    public void Fire(float currentlaunchForce, float fireRate)
    {
            fired = true;
            Rigidbody shellInstance = Instantiate(shell, fireStart.position, fireStart.rotation) as Rigidbody;
            shellInstance.velocity = currentLaunchForce * fireStart.forward;
            shootingAudio.clip = FireClip;
            shootingAudio.Play();
            currentLaunchForce = minLaunchForce;
    }

    //When we get the Damage item have more fireRate shoot like crazy
    float addDamage()
    {
        fireRate -= 2;
        Invoke("ReduceDamage", 10);
        return fireRate;
    }

    //Return our fireRate to normal
    float ReduceDamage()
    {
        fireRate += 2;
        return fireRate;
    }
}
