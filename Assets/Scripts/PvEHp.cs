﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PvEHp : MonoBehaviour
{
    //All Variables
    public Slider slider;
    public int score1 = 1;
    public int hp = 100;
    public GameObject tankExplosion;
    public AudioClip tankExplosionAudio;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Taking Damage In the PVE Stage againsta the enemies
   public void TakeDamage()
    {
        GameObject go = GameObject.Find("ScoreController");
        Score other = (Score)go.GetComponent(typeof(Score));
        if (hp <= 0) return;
        hp -= Random.Range(8, 15);
        slider.value = hp;
        if(hp <= 0)
        {
            other.AddScore(score1);
            AudioSource.PlayClipAtPoint(tankExplosionAudio, transform.position);
            GameObject.Instantiate(tankExplosion, transform.position + Vector3.up, transform.rotation);
            GameObject.Destroy(this.gameObject);
        }
    }
}
