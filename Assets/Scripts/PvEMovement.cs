﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PvEMovement : MonoBehaviour
{
    //All Variables
    public float speed;
    public float angularSpeed;
    public int number = 1;
    public AudioClip idleAudio;
    public AudioClip drivingAudio;
    public AudioSource audio;
    private new Rigidbody rb;
    private PvEAttack attack;

    // Start is called before the first frame update
    void Start()
    {
        //Call in the Components Needed
        rb = this.GetComponent<Rigidbody>();
        audio = this.GetComponent<AudioSource>();
        attack = GetComponent<PvEAttack>();
    }

    // Update is called once per frame
    void Update()
    {
        //When Space is pressed down shoot
        if (Input.GetKeyDown(KeyCode.Space))
        {
            attack.Shoot();
        }
    }

    void FixedUpdate()
    {
        //Get the movement for the players
        float vertical = Input.GetAxis("Verticalplayer" + number);
        rb.velocity = transform.forward * vertical * speed;

        float horizontal = Input.GetAxis("player" + number + "Horizontal");
        rb.angularVelocity = transform.up * horizontal * angularSpeed;

        if(Mathf.Abs(horizontal) > 0.1 || Mathf.Abs(vertical) > 0.1)
        {
            audio.clip = drivingAudio;
            audio.volume = 0.2f;
            if (audio.isPlaying == false)
                audio.Play();
        }

        else
        {
            audio.clip = idleAudio;
            audio.volume = 0.2f;
            if (audio.isPlaying == false)
                audio.Play();
        }

    }
}
