﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float dampTime = 0.2f;         //Time for camera to refocus
    public float screenEdge = 4f;         //Space between the screen edge
    public float minSize = 6.5f;          //The smallest size of the camera
    [HideInInspector] public Transform[] Targets;  //Targets the camera will hold

    private Camera cameraRig;             //Reference the camera
    private float zoomSpeed;              //Reference the speed of the orthographic size
    private Vector3 moveVelocity;         //Reference the velocity of the camera position
    private Vector3 position;             //The position the camera will move

    void Awake()
    {
        //Call in the Component Children in Camera Rig
        cameraRig = gameObject.GetComponentInChildren<Camera>();
    }

    void FixedUpdate()
    {
        //Move and change the position and sze of the camera
        Move();
        Zoom();
    }

    void Move()
    {
        //Average position of the tanks
        AveragePosition();

        //Smooth transition
        transform.position = Vector3.SmoothDamp(transform.position, position, ref moveVelocity, dampTime);
    }

    void AveragePosition()
    {
        Vector3 averagePos = new Vector3();
        int numTargets = 0;

        //Positions of all targets added together
        for(int i = 0; i < Targets.Length; i++)
        {
            //Target not available go to the next one
            if (!Targets[i].gameObject.activeSelf)
                continue;

            //Add the average and increment the number of targets in the average
            averagePos += Targets[i].position;
            numTargets++;
        }

        //Targets divide the sum of the positions by the average
        if (numTargets > 0)
            averagePos /= numTargets;

        //Keep same y value
        averagePos.y = transform.position.y;

        //Desired position equals to average position
        position = averagePos;
    }

    void Zoom()
    {
        //Camera Zooms in the exact numbers needed for taanks
        float size = FindSize();
        cameraRig.orthographicSize = Mathf.SmoothDamp(cameraRig.orthographicSize, size, ref zoomSpeed, dampTime);
    }

   float FindSize()
    {
        //Position the camera rig will move
        Vector3 desiredPos = transform.InverseTransformPoint(position);
        
        //Camera starts at zero
        float size = 0f;

        //Go through all tanks and if not active go to next one
        for(int i = 0; i < Targets.Length; i++)
        {
            if (!Targets[i].gameObject.activeSelf)
                continue;
            Vector3 targetPos = transform.InverseTransformPoint(Targets[i].position);
            Vector3 desiredPosToTarget = targetPos - desiredPos;
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / cameraRig.aspect);
        }

        size += screenEdge;
        size = Mathf.Max(size, minSize);
        return size;
    }

   public void StartPositionAndSize()
   {
        FindSize();
        transform.position = position;
        cameraRig.orthographicSize = FindSize();
   }
}
